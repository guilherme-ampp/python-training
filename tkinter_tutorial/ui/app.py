__author__ = 'galmeida'

from Tkinter import Tk, BOTH, Frame
# widgets that are themed can be imported form the ttk module
from ttk import Button, Style


# this is a container for other widgets in a window
class Example(Frame):
    def __init__(self, parent):
        # calls the constructor of our inherited class
        Frame.__init__(self, parent, background="white")
        # save a reference to our root
        # the parent widget is a Tk root Window in this case
        self.parent = parent
        # we delegate the creation of the window to init_ui() method
        self.init_ui()

    def init_ui(self):
        # sets the title
        self.parent.title("Simple")

        self.style = Style()
        self.style.theme_use("default")
        # the pack method is one of the geometry managers in Tkinter
        # it organizes widgets in horizontal and vertical boxes
        # this could be analogous to the BoxLayout in Java Swing.
        # Here we put the Frame widget, accessed via the self attribute
        # to the Tk root window. It is expanded in both directions,
        # therefore, it takes the whole client space of the root window.
        self.pack(fill=BOTH, expand=1)

        # this is pretty self explanatory =)
        quit_button = Button(self, text="Quit", command=self.quit)
        quit_button.place(x=50, y=50)

        # centers our window by calculating our window size
        # against the screen size and figuring out the proper center position
        self.center_window()

    def center_window(self):
        width = 250
        height = 150

        screen_width = self.parent.winfo_screenwidth()
        screen_height = self.parent.winfo_screenheight()

        x = (screen_width - width) / 2
        y = (screen_height - height) / 2
        self.parent.geometry('%dx%d+%d+%d' % (width, height, x, y))


def main():
    # this creates the root window
    # the root window is the main window in our program
    # it has a title bar and borders
    root = Tk()
    # sets the window size and position on screen
    # 300 and 300 are x and y on screen
    root.geometry("250x150+300+300")

    # instantiate our frame
    app = Example(root)
    # enter the main loop for handling events
    # the main loop receives events from the window system
    # and dispatches them to our widgets
    # it is terminated when we click the close button on our main window
    # or we call the quit() method
    root.mainloop()


if __name__ == "__main__":
    main()