import sys
import core

__author__ = 'galmeida'


def init_argsparse():
    import argparse
    parser = argparse.ArgumentParser(description="Word Counter")
    parser.add_argument('--count', '-c', help="Counts frequency")
    parser.add_argument('--topcount', '-t', type=int, default=20, help="Counts and ranks top")
    parser.add_argument('--ui', action='store_true', help="Launches in UI mode")
    parser.add_argument('filename', type=argparse.FileType(), help="The file with words to be counted")
    return parser


###
# This basic command line argument parsing code is provided and
# calls the print_words() and print_top() functions which you must define.
def run():
    parser = init_argsparse()
    args = parser.parse_args(sys.argv)

    if not args.filename:
        print parser.format_help()
        sys.exit(1)

    option = args.count or args.topcount
    with args.filename as f:
        content = f.read()

    if option == '--count':
        core.print_by_word(content)
    elif option == '--topcount':
        core.print_by_rank(content)
    else:
        print 'unknown option: ' + option
        sys.exit(1)