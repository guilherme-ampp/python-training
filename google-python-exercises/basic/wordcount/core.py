from collections import Counter
import re

__author__ = 'galmeida'


def load_words(content):
    return [word.lower().strip() for word in re.findall("\w+", content)]


def count_words(words):
    # takes a list and counts how many occurrences of equal elements
    # and returns a dictionary
    return Counter(words)


def print_by_word(content):
    words = load_words(content)
    d = sorted([(k, v) for k, v in count_words(words).items()])

    return __print_words(d)


def print_by_rank(content):
    words = load_words(content)
    d = sorted([(k, v) for k, v in count_words(words).items()], key=lambda j: j[-1], reverse=True)

    return __print_words(d)


def __print_words(iterator):
    # for word, count in iterator:
    #     print ': '.join([word, count])

    printed_words = ''
    for word, count in iterator:
        printed_words = ''.join([printed_words, word, ' - ', str(count), '\n'])

    return printed_words


