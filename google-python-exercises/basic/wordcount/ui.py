__author__ = 'galmeida'

import Tkinter as tk
import tkFileDialog
import ttk
import core


class WordCountUI(tk.Frame):

    MODES = {
        1: core.print_by_word,
        2: core.print_by_rank
    }

    def __init__(self, parent):
        tk.Frame.__init__(self, parent, background="white")
        self.parent = parent
        self.style = ttk.Style()
        self.option_selected = tk.IntVar()
        self.filename_txt = tk.Entry(self)
        self.scrolled_text = ScrolledText(self)

        self.init_ui()

    def init_ui(self):
        self.parent.title("Word Count")
        self.style.theme_use("default")
        self.center_window()
        self.pack(fill=tk.BOTH, expand=1)

        self.filename_txt.place(x=10, y=15)

        self.scrolled_text.place(x=10, y=85)

        radio_option_count = tk.Radiobutton(self, text="Count", var=self.option_selected, value=1)
        radio_option_count.place(x=10, y=45)
        radio_option_top_count = tk.Radiobutton(self, text="Top Count", var=self.option_selected, value=2)
        radio_option_top_count.place(x=75, y=45)

        run_button = tk.Button(self, text="Run", command=self.run)
        run_button.place(x=190, y=14)
        run_button = tk.Button(self, text="Browse", command=self.search_file)
        run_button.place(x=245, y=14)

    def center_window(self):
        width = 400
        height = 500

        screen_width = self.parent.winfo_screenwidth()
        screen_height = self.parent.winfo_screenheight()

        x = (screen_width - width) / 2
        y = (screen_height - height) / 2
        self.parent.geometry('%dx%d+%d+%d' % (width, height, x, y))

    def get_filename(self):
        return self.filename_txt.get()

    def search_file(self):
        filename = tkFileDialog.askopenfilename()
        self.filename_txt.insert(tk.END, filename)

    def run(self):
        filename = self.get_filename()

        with open(filename) as f:
            content = f.read()

        func = WordCountUI.MODES[self.option_selected.get()]
        self.scrolled_text.set_text(func(content))


class ScrolledText(tk.Text):

    def __init__(self, master):
        tk.Text.__init__(self, master=master)
        self.config(state=tk.NORMAL)
        self.pack(fill=tk.BOTH, expand=1)
        scroll = tk.Scrollbar()
        scroll.configure(command=self.yview)

    # @property
    def text(self):
        return self.get(tk.BEGIN, tk.END)

    # @text.setter
    def set_text(self, content):
        self.delete(1.0, tk.END)
        self.insert(tk.END, content)


def launch_ui():
    root = tk.Tk()
    app = WordCountUI(root)
    root.mainloop()

if __name__ == "__main__":
    launch_ui()
