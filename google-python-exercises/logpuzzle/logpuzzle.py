#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import os
import re
import sys
import urllib

"""Logpuzzle exercise
Given an apache logfile, find the puzzle urls and download the images.

Here's what a puzzle url looks like:
10.254.254.28 - - [06/Aug/2007:00:13:48 -0700] "GET /~foo/puzzle-bar-aaab.jpg HTTP/1.0" 302 528 "-" "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6"
"""

import argparse
import re

HTML_HEAD = "<head></head>"
HTML_BODY_BEGIN = "<body>"
HTML_BODY_END = "</body>"
PATTERN = re.compile(r'.+GET (.+?\.jpg).+')


def load_log_content(filename):
    with open(filename) as f:
        content = f.read()

    return content


def sort_urls(urls):
    return sorted(urls, key=lambda url: url.split('-')[-1])


def read_urls(log_content):
    """Returns a list of the puzzle urls from the given log file,
    extracting the hostname from the filename itself.
    Screens out duplicate urls and returns the urls sorted into
    increasing order."""
    urls = PATTERN.findall(log_content)
    return sort_urls([t for t in set(urls) if 'puzzle' in t])


def download_images(host, img_urls, dest_dir):
    """Given the urls already in the correct order, downloads
    each image into the given directory.
    Gives the images local filenames img0, img1, and so on.
    Creates an index.html in the directory
    with an img tag to show each local image file.
    Creates the directory if necessary.
    """
    host = "http://" + host
    index_html = ''.join(['<img src=%s>' % (host + url) for url in img_urls])
    index_html = ''.join([HTML_HEAD, HTML_BODY_BEGIN, index_html, HTML_BODY_END])

    with open(dest_dir, "w+") as f:
        f.write(index_html)


def main():
    parser = argparse.ArgumentParser(description="Log Puzzle")
    parser.add_argument('--todir', '-d', dest='directory', help="The download images directory")
    parser.add_argument('logfile', help="The log file")
    args = parser.parse_args(sys.argv[1:])

    if not args.logfile:
        print parser.format_help()
        sys.exit(1)

    log = load_log_content(args.logfile)
    host = args.logfile.split('_')[1]
    img_urls = read_urls(log)

    print 'Image URLs:\n'
    print '\n'.join(img_urls)
    if args.directory:
        download_images(host, img_urls, args.directory)
        print "Done creating " + args.directory


if __name__ == '__main__':
    main()
