__author__ = 'galmeida'

from vector import Vector


def test():
    assert Vector(2, 4)
    assert repr(Vector(2, 4)) == 'Vector(2,4)'
    assert str(Vector(2, 4)) == '(2, 4)'
    assert Vector(2, 4) == Vector(2, 4)
    assert Vector(2, 4) + Vector(1, 2) == Vector(3, 6)
    assert Vector(2, 4) * 2 == Vector(4, 8)
    assert abs(Vector(3, 4)) == 5.0
    assert not bool(Vector(0, 0))
    assert bool(Vector(0, 1)) == bool(Vector(1, 0)) == bool(Vector(1, 1))
    assert Vector(2, 2).dot(Vector(3, 4)) == 14

test()