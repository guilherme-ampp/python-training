__author__ = 'galmeida'

from math import sqrt
from numbers import Integral
from math import hypot


class Vector(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return '%s(%d,%d)' % (self.__class__.__name__, self.x, self.y)

    def __str__(self):
        return '(%d, %d)' % (self.x, self.y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __add__(self, other):
        return Vector(self.x + other.x, self.y + other.y)

    def __mul__(self, scalar):
        if not isinstance(scalar, Integral):
            raise Exception("Expecting Integral got " + scalar)

        return Vector(self.x * scalar, self.y * scalar)

    def __abs__(self):
        # return sqrt(abs(self.x ** 2 + self.y ** 2))
        return hypot(self.x, self.y)

    def __nonzero__(self):
        # works for python 2, for python 3 we would have to enclose integers in bool()
        return self.x or self.y

    def dot(self, other):
        return (self.x * other.x) + (self.y * other.y)