__author__ = 'galmeida'

from deck_refactor import Card, Deck


def test():
    card = Card('A', 'spades')
    assert card.rank == 'A'
    assert card.suit == 'spades'
    assert card == Card('A', 'spades')
    assert str(card) == "(A, spades)"

    deck = Deck()

    assert deck
    assert len(deck) == 52
    assert deck[0] == Card(rank='2', suit='spades')
    assert deck[:2] == [Card('2', 'spades'), Card('3', 'spades')]
    assert deck[12::13] == [Card('A', 'spades'), Card('A', 'diamonds'), Card('A', 'clubs'), Card('A', 'hearts')]
    assert Card('Q', 'hearts') in deck

test()