__author__ = 'galmeida'


class Card(object):

    def __init__(self, rank, suit):
        self.rank = rank
        self.suit = suit

    def __eq__(self, other):
        return self.rank == other.rank and self.suit == other.suit

    # returns a string
    def __repr__(self):
        # %s uses __str__
        # %r uses __repr__
        return "Card(%r, %r)" % (self.rank, self.suit)

    def __str__(self):
        return '(%s, %s)' % self


class Deck(object):

    def __init__(self):
        ranks = [str(a) for a in range(2, 11)] + list('JQKA')
        suits = ['spades', 'diamonds', 'clubs', 'hearts']
        self.cards = [Card(r, s) for s in suits for r in ranks]

    def __len__(self):
        return len(self.cards)

    def __getitem__(self, item):
        if isinstance(item, slice):
            start = 0 if not item.start else item.start
            step = 1 if not item.step else item.step
            stop = item.stop if item.stop else len(self.cards)
            the_item = self.cards[start:stop:step]
        else:
            the_item = self.cards[item]

        return the_item

    def __contains__(self, item):
        return item in self.cards
